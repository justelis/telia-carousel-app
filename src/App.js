import React, {useRef, useEffect} from 'react';
import cat1 from './images/cat1.jpg';
import cat2 from './images/cat2.jpg';
import cat3 from './images/cat3.jpg';
import cat4 from './images/cat4.jpg';
import cat5 from './images/cat5.jpg';
import './App.css';


function App() {

  const carouselRef = useRef(null);

  let imgArray = new Array();

  imgArray[0] = new Image();
  imgArray[0].src = cat1;
  imgArray[0].alt = 'cat1';

  imgArray[1] = new Image();
  imgArray[1].src = cat2;
  imgArray[1].alt = 'cat2';

  imgArray[2] = new Image();
  imgArray[2].src = cat3;
  imgArray[2].alt = 'cat3';

  imgArray[3] = new Image();
  imgArray[3].src = cat4;
  imgArray[3].alt = 'cat4';

  imgArray[4] = new Image();
  imgArray[4].src = cat5;
  imgArray[4].alt = 'cat5';

  useEffect(() => {
    if (carouselRef.current) {
      carouselRef.current.images = imgArray;
    }
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <h1>Carousel Test App</h1>
        <telia-carousel ref={carouselRef} size="md" height="50vh" style={{maxWidth: '800px', width: '100%'}}/>
      </header>
    </div>
  );
}

export default App;
